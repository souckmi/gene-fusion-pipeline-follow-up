#!/usr/bin/env nextflow

// Using DSL-2
nextflow.enable.dsl=2

process merge_input_files {

    publishDir "$params.outdir/merged_input_files", mode: 'copy'

    input:
    path readsdir

    output:
    path '*.gz'

    """
    merge_fastq_files.py -i ${readsdir}
    """
}