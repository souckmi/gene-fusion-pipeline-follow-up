#!/usr/bin/env nextflow

nextflow.enable.dsl=2

include { fusion_detection } from './Fusions-Module/modules'
include { merge_input_files } from './Helpers-Module/modules'


def helpMessage() {
    log.info"""
Usage local: 

nextflow run WORKFLOW <ARGUMENTS>

Usage K8S:

nextflow kuberun https://gitlab.com/souckmi/gene-fusion-thesis-wsl2 \
                -r <REVISION> \
                -pod-image 'cerit.io/nextflow/nextflow:22.06.1' \
                -v pvc-samba-brno12-souckmi-gene:/mnt \
                <ARGUMENTS>

Required Arguments:

--readsdir          Directory containing input files
                    Files should follow pattern "*{1,2}.fastq.gz"
--reference         Directory containing required references
--outdir            Directory for output files
--runId             Run ID
--referenceVersion  Currently only 'hg38' is supported and set as default

    """.stripIndent()
}

// Main workflow
workflow {

    /* 
    * prints user convenience 
    */
    log.info"""
    "G E N E   F U S I O N S   P I P E L I N E    "
    "============================================="
    "reads directory     : ${params.readsdir}"
    "reference directory : ${params.reference}"
    "output directory    : ${params.outdir}"
    "run id              : ${params.runId}"
    """


    // Show help message if the user specifies the --help flag at runtime
    // or if any required params are not provided
    if ( params.help || params.outdir == false || params.reference == false || params.readsdir == false){
        helpMessage()
        exit 1
    }

    if (params.mergeInputFiles){
        fastqs_ch = Channel.fromPath( params.readsdir, checkIfExists: true).view()
        merge_input_files(fastqs_ch)
        read_pairs_ch = merge_input_files
                            .out
                            .flatten()
                            .map { it -> [it.name.split('_')[0], it] }
                            .groupTuple()
    }
    else{
        Channel
        .fromFilePairs("$params.readsdir/*{_R1,_R2}.fastq.gz")
        .ifEmpty { error "No files found matching the pattern $params.readsdir/*{_R1,_R2}.fastq.gz"}
        .set {read_pairs_ch}

    }
    
    fusion_detection(read_pairs_ch)


}