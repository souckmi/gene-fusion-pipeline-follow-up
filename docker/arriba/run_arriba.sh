#!/bin/bash

# tell bash to be verbose and to abort on error
set -o pipefail
set -x -e -u

# get arguments
STAR_BAM_FILE="$1"
ANNOTATION_GTF="$2"
ASSEMBLY_FA="$3"
BLACKLIST_TSV="$4"
KNOWN_FUSIONS_TSV="$5"
TAGS_TSV="$KNOWN_FUSIONS_TSV" # different files can be used for filtering and tagging, but the provided one can be used for both
PROTEIN_DOMAINS_GFF3="$6"
THREADS="$7"

# find installation directory of arriba
BASE_DIR=$(dirname "$0")

"$BASE_DIR/arriba" \
	-x "$STAR_BAM_FILE" \
	-o fusions.tsv -O fusions.discarded.tsv \
	-a "$ASSEMBLY_FA" -g "$ANNOTATION_GTF" -b "$BLACKLIST_TSV" -k "$KNOWN_FUSIONS_TSV" -t "$TAGS_TSV" -p "$PROTEIN_DOMAINS_GFF3" \
#	-d structural_variants_from_WGS.tsv

# sorting and indexing is only required for visualization
if [[ $(samtools --version-only 2> /dev/null) =~ ^1\. ]]; then
	samtools sort -@ "$THREADS" -m $((40000/THREADS))M -T tmp -O bam Aligned.out.bam > Aligned.sortedByCoord.out.bam
	rm -f Aligned.out.bam
	samtools index Aligned.sortedByCoord.out.bam
else
	echo "samtools >= 1.0 required for sorting of alignments" 1>&2
fi
