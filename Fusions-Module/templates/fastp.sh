#!/bin/bash

mkdir -p fastp
fastp -i ${reads[0]} -I ${reads[1]} \
      -o "fastp/demultiplexed_trimmed_${reads[0].simpleName}.fastq.gz" \
      -O "fastp/demultiplexed_trimmed_${reads[1].simpleName}.fastq.gz" \
      -D \
      -h "fastp/${pair_id}_${params.runId}_${params.referenceVersion}_fastp_report.html" \
      --thread ${task.cpus}
