#!/bin/bash

bpipe run -p refBase=${params.reference+"/JAFFA"} \
          -p genome=hg38 \
          -n ${task.cpus} \
          /opt/JAFFA/JAFFA_direct.groovy ${reads1} ${reads2}
