#!/bin/bash

Rscript /arriba*/draw_fusions.R --annotation=${params.reference}/STAR-Fusion/STARFusion/GRCh38_gencode_v37_CTAT_lib_Mar012021.plug-n-play/ctat_genome_lib_build_dir/ref_annot.gtf \
                                --cytobands=${params.reference}/Arriba2/database/cytobands_hg38_GRCh38_v2.3.0.tsv \
                                --proteinDomains=${params.reference}/Arriba2/database/protein_domains_hg38_GRCh38_v2.3.0.gff3 \
                                --fusions=${fusions} \
                                --output=fusions.pdf \
                                --alignments=${bam}